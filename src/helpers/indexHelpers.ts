import Immutable from "immutable";

export const UpperCaseLetterStart = 65;
export const aToZLength = 25;

export const getHorizontalBlock = (block: string) =>
  block.match(/-?\d*\.{0,1}\d+/g).join("");

export const getVerticalBlock = (block: string) =>
  block.match(/[^0-9]+/g).join("");

export const fromCharCode = (charCode: number) => {
  const firstLetter = Math.floor(charCode / aToZLength);
  const secondLetter =
    firstLetter !== 0 ? charCode - aToZLength * firstLetter : charCode;
  return String.fromCharCode(
    UpperCaseLetterStart + firstLetter,
    UpperCaseLetterStart + secondLetter
  );
};

export const fromCodeChar = (codeChar: string) => {
  const firstLetter = codeChar.charCodeAt(0) - UpperCaseLetterStart;
  if (firstLetter < 1) {
    return codeChar.charCodeAt(1) - UpperCaseLetterStart;
  }

  return (
    firstLetter * aToZLength + (codeChar.charCodeAt(1) - UpperCaseLetterStart)
  );
};

export const changeBlockByVertical = (
  block: Immutable.Map<string, any>,
  direction: string
) => {
  const verticalBlockCode: number = fromCodeChar(block.get("vertical"));

  return (
    fromCharCode(verticalBlockCode + (direction === "S" ? 1 : -1)) +
    block.get("horizontal")
  );
};

export const changeBlockByHorizontal = (
  block: Immutable.Map<string, any>,
  direction: string
) => {
  console.log(direction, "direction");
  console.log(block.get("horizontal") - 1, 'block.get("horizontal")');

  return (
    block.get("vertical") +
    (parseInt(block.get("horizontal")) + (direction === "E" ? 1 : -1))
  );
};
