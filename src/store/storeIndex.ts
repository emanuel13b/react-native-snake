import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers/reducersIndex";
import { createLogger } from "redux-logger";
import Immutable, { fromJS, Iterable } from "immutable";

const stateTransformer = (state: Immutable.Map<string, any>) => {
  if (Iterable.isIterable(state)) return state.toJS();
  else return state;
};

const logger: object = createLogger({
  stateTransformer
});

const initialState = fromJS({
  grid: 30,
  snake: [
    "AW11",
    "AW10",
    "AW9",
    "AW8",
    "AW7",
    "AW6",
    "AW5",
    "AW4",
    "AW3",
    "AW2",
    "AW1",
    "AW0"
  ]
});

const enhancers: Array<any> = [];
const middleware: Array<any> = [logger];

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

export default createStore(rootReducer, initialState, composedEnhancers);
