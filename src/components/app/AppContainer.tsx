import React, { Component } from "react";
import { App } from "./App";
import store from "../../store/storeIndex";

interface AppContainerInterface {}

class AppContainer extends Component<AppContainerInterface> {
  render() {
    return <App />;
  }
}

export default AppContainer;
