import React, { Component } from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import GridBoard from "../gridboard";
import SnakeControls from "../snakeControls/index";
import Controls from "../controls/index";

interface Props {}

export const App = (props: Props) => {
  return (
    <View>
      <Controls />
      <GridBoard />
      <SnakeControls />
    </View>
  );
};
