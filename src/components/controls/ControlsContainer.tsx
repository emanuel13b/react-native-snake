import React, { Component } from "react";
import { connect } from "react-redux";
import { Controls } from "./Controls";
import * as Actions from "../../actions/actionsIndex";

type Props = {};

class ControlsContainer extends Component<Props> {
  render() {
    return <Controls {...this.props} />;
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {
  startGame: Actions.startGame
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlsContainer);
