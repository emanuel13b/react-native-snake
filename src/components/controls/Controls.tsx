import React from "react";
import { View, Button } from "react-native";

export const Controls = props => {
  return (
    <View>
      <Button title="Start the Game" onPress={props.startGame} />
    </View>
  );
};
