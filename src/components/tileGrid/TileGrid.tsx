import { View, Text, StyleSheet, Dimensions } from "react-native";
import React from "react";

export const TileGrid = props => {
  return (
    <View
      style={style(props.rowNr + props.columnNr, props.snake, props.gridSize)}
    />
  );
};

const style = (block, snake, size) => {
  const partOfSnake = isSnake(block, snake);
  const { width } = Dimensions.get("window");
  const tileSize = Math.floor(width / size);

  return {
    height: tileSize,
    width: tileSize,
    margin: partOfSnake ? 0 : 0,
    backgroundColor: partOfSnake
      ? isHead(block, snake) ? "#16a085" : "#2ecc71"
      : "#bdc3c7"
  };
};

const isSnake = (block, snake) => snake.indexOf(block) >= 0;
const isHead = (block, snake) => snake.indexOf(block) === 0;
