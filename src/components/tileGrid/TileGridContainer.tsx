import React, { Component } from "react";
import { connect } from "react-redux";
import { TileGrid } from "./TileGrid";
import * as Selectors from "../../selectors/selectorsIndex";

type Props = {};

class TileGridContainer extends Component<Props> {
  render() {
    return <TileGrid {...this.props} />;
  }
}

const mapStateToProps = state => {
  return {
    snake: Selectors.snake(state),
    gameStarted: Selectors.gameStarted(state),
    gridSize: Selectors.gridSize(state)
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(TileGridContainer);
