import { View, Text, StyleSheet } from "react-native";
import React from "react";
import TileGrid from "../tileGrid";

interface InterfaceGridRowProps {
  key?: string;
  size: number;
  rowNr: string;
}

export const GridRow = (props: InterfaceGridRowProps) => {
  return (
    <View>
      <View style={styles.grid}>{renderTiles(props.size, props.rowNr)}</View>
    </View>
  );
};

const renderTiles = (size, rowNr) => {
  return new Array(size).fill(undefined).map((item, idx) => {
    return <TileGrid key={"tile" + idx} columnNr={idx} rowNr={rowNr} />;
  });
};

const styles = StyleSheet.create({
  grid: {
    flexDirection: "row",
    flexWrap: "wrap"
  }
});
