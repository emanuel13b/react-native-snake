import React, {Component} from 'react'
import * as Actions from '../../actions/actionsIndex'
import * as Selectors from '../../selectors/selectorsIndex';
import {connect} from 'react-redux';
import {SnakeControls} from './SnakeControls';

type Props = {};

class SnakeControlsContainer extends Component<Props> {
  render() {
    return <SnakeControls {...this.props}/>
  }
}

const mapStateToProps = (state) => {
  return {
    snakeDirection: Selectors.getDirection(state),
  }
};

const mapDispatchToProps = {
  changeDirection: Actions.changeDirection,
};

export default connect(mapStateToProps, mapDispatchToProps)(SnakeControlsContainer);