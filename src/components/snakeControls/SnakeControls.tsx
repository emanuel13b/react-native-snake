import React from 'react';
import {View, Button, StyleSheet, Text} from 'react-native';

export const SnakeControls = (props) => {
  return (
    <View>
      <Button title='Left' onPress={() =>props.changeDirection('W')}/>
      <Button title='Top' onPress={() =>props.changeDirection('N')}/>
      <Button title='Right' onPress={() =>props.changeDirection('E')}/>
      <Button title='Down' onPress={() =>props.changeDirection('S')}/>
      <Text>
        Direction {props.snakeDirection}
      </Text>

      {/*<View style={styles.dPad}>*/}
        {/*<View><Text>&#9650;</Text></View>*/}
        {/*<View><Text>&#9654;</Text></View>*/}
        {/*<View><Text>&#9660;</Text></View>*/}
        {/*<View><Text>&#9664;</Text></View>*/}
      {/*</View>*/}
    </View>
  )
};

const styles = StyleSheet.create({
  dPad: {
    width: 30,
    height: 30,
    position: 'relative',
  },
  d: {
    width: '1em',
    height: '1em',
    position: 'absolute',
  }
});