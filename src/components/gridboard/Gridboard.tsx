import { View, StyleSheet } from "react-native";
import React from "react";
import { GridRow } from "../gridRow/GridRow";

import * as Helpers from "../../helpers/indexHelpers";
import { connect } from "react-redux";
import { InterfaceGridboardContainerProps } from "./GridboardContainer";

export const GridBoard = (props: InterfaceGridboardContainerProps) => {
  return (
    <View style={gridBoardStyle.wrapper}>
      <View style={containerStyle(props.gridSize)}>
        {createRows(props.gridSize)}
      </View>
    </View>
  );
};

function createRows(size) {
  return new Array(size)
    .fill(undefined)
    .map((itm, idx) => (
      <GridRow
        key={"row" + idx}
        rowNr={Helpers.fromCharCode(idx)}
        size={size}
      />
    ));
}

const containerStyle = size => {
  return {
    backgroundColor: "#bdc3c7",
    width: size * 12,
    height: size * 12
  };
};

const gridBoardStyle = StyleSheet.create({
  wrapper: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20
  }
});
