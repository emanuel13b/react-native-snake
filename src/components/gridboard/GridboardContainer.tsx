import React, { Component } from "react";
import { GridBoard } from "./Gridboard";
import { connect } from "react-redux";
import * as Actions from "../../actions/actionsIndex";
import * as Selectors from "../../selectors/selectorsIndex";

export interface InterfaceGridboardContainerProps {
  moveSnake: () => void;
  initApp: () => void;
  gameStarted: boolean;
  gridSize: number;
}

interface InterfaceGridboardContainerState {
  intervalId: any;
}

class GridboardContainer extends Component<
  InterfaceGridboardContainerProps,
  InterfaceGridboardContainerState
> {
  private constructor(props: InterfaceGridboardContainerProps) {
    super(props);

    this.state = {
      intervalId: undefined
    };

    this.startGame = this.startGame.bind(this);
  }

  public componentWillReceiveProps(newProps) {
    if (this.props.gameStarted !== newProps.gameStarted) {
      if (newProps.gameStarted) {
        let intervalId = setInterval(this.startGame, 200);
        this.setState({ intervalId: intervalId });
      }

      if (!newProps.gameStarted) {
        clearInterval(this.state.intervalId);
      }
    }
  }

  public componentWillMount() {
    this.props.initApp();
  }

  public render() {
    return <GridBoard {...this.props} />;
  }

  private startGame() {
    this.props.moveSnake();
  }
}

const mapStateToProps = state => {
  return {
    snake: Selectors.snake(state),
    gameStarted: Selectors.gameStarted(state),
    gridSize: Selectors.gridSize(state)
  };
};

const mapDispatchToProps = {
  moveSnake: Actions.moveSnake,
  initApp: Actions.initApp
};

export default connect(mapStateToProps, mapDispatchToProps)(GridboardContainer);
