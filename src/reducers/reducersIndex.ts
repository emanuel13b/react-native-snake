import * as Types from "../actions/typesIndex";
import Immutable, { fromJS } from "immutable";
import * as Helpers from "../helpers/indexHelpers";
import * as Selectors from "../selectors/selectorsIndex";

export interface InterfaceAction {
  type: string;
  payload?: any;
}

export default (state = fromJS({}), action: InterfaceAction) => {
  switch (action.type) {
    case Types.INIT_APP:
      return state;

    case Types.START_GAME:
      return state.set("direction", "E").set("gameStarted", true);

    case Types.MOVE_SNAKE: {
      const snake = Selectors.snake(state);
      const snakeHead = snake.get(0);
      const direction = Selectors.getDirection(state);

      let snakeBlock = fromJS({
        horizontal: Helpers.getHorizontalBlock(snakeHead),
        vertical: Helpers.getVerticalBlock(snakeHead)
      });

      let nextBlock: string;

      switch (direction) {
        case "N":
        case "S":
          nextBlock = Helpers.changeBlockByVertical(snakeBlock, direction);
          break;

        case "E":
        case "W": {
          nextBlock = Helpers.changeBlockByHorizontal(snakeBlock, direction);
          break;
        }

        default:
          break;
      }

      const nextBlockImm = fromJS({
        horizontal: Helpers.getHorizontalBlock(nextBlock),
        vertical: Helpers.getVerticalBlock(nextBlock)
      });

      //Collision with walls detection;
      if (
        topWallDetection(nextBlockImm) ||
        bottomWallDetection(nextBlockImm, state) ||
        leftWallDetection(nextBlockImm, direction) ||
        rightWallDetection(nextBlockImm, direction, state) ||
        snakeTailDetection(nextBlockImm, state)
      ) {
        alert("Game over!");
        state = state.set("gameStarted", false);
      } else {
        let newSnake = snake.splice(-1, 1).insert(0, nextBlock);
        state = state.set("snake", newSnake);
      }

      return state;
    }

    case Types.CHANGE_DIRECTION:
      return state.set("direction", action.payload);

    default:
      return state;
  }
};

const topWallDetection = (block: Immutable.Map<string, any>): boolean => {
  return (
    block.get("vertical").charCodeAt(0) < Helpers.UpperCaseLetterStart ||
    block.get("vertical").charCodeAt(1) < Helpers.UpperCaseLetterStart
  );
};

const bottomWallDetection = (
  block: Immutable.Map<string, any>,
  state: Immutable.Map<string, any>
): boolean => {
  return (
    Helpers.fromCodeChar(block.get("vertical")) >= Selectors.gridSize(state)
  );
};

const leftWallDetection = (
  block: Immutable.Map<string, any>,
  direction: string
): boolean => {
  console.log(block.toJS());
  return parseInt(block.get("horizontal")) < 0 && direction === "W";
};

const rightWallDetection = (
  block: Immutable.Map<string, any>,
  direction: string,
  state: Immutable.Map<string, any>
): boolean => {
  return (
    parseInt(block.get("horizontal")) >= Selectors.gridSize(state) &&
    direction === "E"
  );
};

const snakeTailDetection = (
  block: Immutable.Map<string, any>,
  state: Immutable.Map<string, any>
): boolean => {
  const snake = Selectors.snake(state);
  return snake.includes(block.get("vertical") + block.get("horizontal"));
};
