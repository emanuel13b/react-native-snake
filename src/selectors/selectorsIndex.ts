import Immutable from "immutable";

export const gridSize = state => state.get("gridSize", 30);
export const snake = state => state.get("snake", Immutable.List());
export const gameStarted = state => state.get("gameStarted", false);
export const getDirection = state => state.get("direction", undefined);
