export const INIT_APP = 'INIT_APP';

export const START_GAME = 'START_GAME';
export const MOVE_SNAKE = 'MOVE_SNAKE';
export const CHANGE_DIRECTION = 'CHANGE_DIRECTION';