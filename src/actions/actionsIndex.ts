import * as Types from "./typesIndex";

export const initApp = () => ({ type: Types.INIT_APP });

export const startGame = () => ({ type: Types.START_GAME });
export const moveSnake = () => ({ type: Types.MOVE_SNAKE });
export const changeDirection = direction => ({
  type: Types.CHANGE_DIRECTION,
  payload: direction
});
