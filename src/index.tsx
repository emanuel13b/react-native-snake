import { AppRegistry } from "react-native";
import App from "./components/app";
import store from "./store/storeIndex";
import React, { Component } from "react";
import { Provider } from "react-redux";

interface RootInterface {
  store: object;
}

class Root extends Component<RootInterface> {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

AppRegistry.registerComponent("toDoAppNative", () => Root);
